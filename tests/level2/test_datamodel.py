from __future__ import annotations
from atila import datamodel
from atila.app.parameters import ArgSpec, Parameters
from django_.myapp.models import MyModel, Type, Tag
from pprint import pprint
import pytest
import confutil
import os
import json

def test_argspec (app):
    class Pet:
        age: list
        color: str = 'any'

    f = ArgSpec (Pet)
    assert f.specs ['lists'] == ['age']
    assert f.specs ['strs'] == ['color']
    assert f.defaults ['color'] == 'any'
    assert 'age' not in f.defaults
    pprint (f.fields)
    pprint (f.specs)
    pprint (f.defaults)


def test_argspec_related_models (app):
    @datamodel (MyModel)
    class Pet:
        age: list
        color: str = 'any'
        breed = None
        num__lte = 200
        name__iregex = 'Han[sx]'
        def blankable__computed (context, val):
            return 'not blank'

    f = ArgSpec (Pet)
    assert f.defaults ['color'] == 'any'
    assert 'age' not in f.defaults

    assert f.specs ['strs'] == ['name', 'nullable', 'blankable', 'choicable', 'text', 'color']
    assert f.specs ['ints'] == ['num']
    assert f.specs ['required'] == ['name', 'num', 'age']

    assert f.defaults ['nullable'] == 'k'
    assert f.specs ['blankable__len__lte'] == 16
    assert f.specs ['choicable__codein'] == [('b', 'b'), ('a', 'a')]
    assert hasattr (f.specs ['email__search'], 'pattern')
    assert f.fields == set (f.specs ['strs'] + ['num', 'color', 'age', 'breed', 'email', 'image'])
    assert 'type' not in f.fields

    pprint (f.fields)
    pprint (f.specs)
    pprint (f.defaults)

    p = Parameters ()
    assert p._validate_param ({"age": 1}, **f.specs) == 'parameter name is required'
    assert p._validate_param ({"age": 1, 'name': 4}, **f.specs) == 'parameter num is required'
    assert p._validate_param ({"age": 1, 'name': 4, 'num': 100}, **f.specs) == 'parameter name should be str type'

    assert p._validate_param ({"age": 1, 'name': 'Hans', 'num': 100}, **f.specs) == 'parameter age should be a list or comma/bar delimetered string'
    assert not p._validate_param ({"age": [1, 2], 'name': 'Hans', 'num': 100}, **f.specs)

    payload = {"age": [1, 2], 'name': 'Hans', 'num': 100}
    payload ['choicable'] = 'c'
    assert p._validate_param (payload, **f.specs).find ('parameter choicable should be one of') == 0

    payload ['choicable'] = 'b'
    payload ['nullable'] = 'b' * 17
    assert p._validate_param (payload, **f.specs) == 'parameter length of nullable should less than or equal to 16'

    payload ['nullable'] = 'bbbb'
    payload ['num'] = 300
    assert p._validate_param (payload, **f.specs) == 'parameter num should less than or equal to 200'

    payload ['name'] = 'hans'
    payload ['num'] = 200
    assert not p._validate_param (payload, **f.specs)

    payload ['name'] = 'Hant'
    payload ['num'] = 100
    assert p._validate_param (payload, **f.specs) == 'parameter name should match with regular expression Han[sx] with case insensitivity'

    payload ['name'] = 'Hans'
    payload ['blankable'] = ''

    assert not p._validate_param (payload, **f.specs)
    assert payload ['blankable'] == 'not blank'


    @datamodel (MyModel)
    class Pet2:
        age: list
        color: str = 'any'
        num__lte = 200
        name__iregex = 'Han[sx]'
        def blankable__computed (context, val):
            return 'not blank'

    os.environ ["ATILA_SET_SEPC"] = "1"
    @app.post ("/pets5")
    @app.argspec (Pet2)
    def pets (context, **payload):
        assert payload ['nullable'] == 'k'
        return context.API ()

    @app.put ("/pets7")
    @app.argspec (Pet2)
    def pets7 (context, **payload):
        assert payload ['nullable'] == 'k'
        return context.API ()

    @app.patch ("/pets8")
    @app.argspec (Pet2)
    def pets8 (context, **payload):
        assert 'nullable' not in payload
        return context.API ()

    @app.patch ("/pets9/<id>")
    @app.argspec (Pet2)
    def pets9 (context, id, **payload):
        assert 'nullable' not in payload
        return context.API ()

    with app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.put ("/pets7", json = {"age": [1, 2], 'name': 'Hans'})
        assert resp.status_code == 400

        resp = cli.put ("/pets7", json = {"age": [1, 2], 'name': 'Hans', 'num': 100})
        assert resp.status_code == 200

        resp = cli.post ("/pets5", json = {"age": [1, 2], 'name': 'Hans'})
        assert resp.status_code == 400

        resp = cli.post ("/pets5", json = {"age": [1, 2], 'name': 'Hans', 'num': 100})
        aa = resp.data
        assert resp.status_code == 200

        resp = cli.post ("/pets5", json = {"age": [1, 2], 'name': 'Hans', 'num': 100})
        assert resp.data == aa
        assert resp.status_code == 200
        assert 'parameter_requirements' in aa ['__spec__']
        assert app._get_parameter_requirements ('pets') ['ARGS']['blankable'] == {'len__lte': 16, 'type': 'str', 'computed': 'blankable__computed'}
        json.dumps (app._get_parameter_requirements ('pets'))

        # IMP: datamodel PATCH does not check required and not set default
        resp = cli.patch ("/pets8", json = {"age": [1, 2], 'name': 'Hans'})
        assert resp.status_code == 200

        resp = cli.patch ("/pets8", json = {'name': 'Hans'})
        assert resp.status_code == 200

        resp = cli.patch ("/pets9/100", json = {'name': 'Hans'})
        assert resp.status_code == 200

    del os.environ ["ATILA_SET_SEPC"]


def test_argspec_related_models_with_func_args (app):
    @datamodel (MyModel)
    class Pet2:
        age: list
        color: str = 'any'
        num__lte = 200
        name__iregex = 'Han[sx]'
        def blankable__computed (context, val):
            return 'not blank'

    assert Pet2.__required__ == ['name', 'num']

    @app.route ("/pets5/<uid>")
    @app.argspec (Pet2)
    def pets (context, uid: str, **payload):
        return context.API ()

    req = app._get_parameter_requirements ('pets')
    assert 'uid' in req ['ARGS']
    assert req ['ARGS']['uid']['required']

    @app.route ("/pets6/<uid>")
    def pets6 (context, uid: str, **payload):
        return context.API ()

    req = app._get_parameter_requirements ('pets6')
    assert 'uid' in req ['ARGS']
    assert req ['ARGS']['uid']['required']

    with pytest.raises (AssertionError):
        @app.route ("/pets7/<uid>")
        def pets7 (context, **payload):
            return context.API ()

    req = app._get_parameter_requirements ('pets7')
    assert 'uid' not in req ['ARGS']

    with app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.post ("/pets5", json = {"age": [1, 2], 'name': 'Hans', 'num': 100})
        assert resp.status_code == 404
        resp = cli.post ("pets5/400", json = {"age": [1, 2], 'name': 'Hans', 'num': 100})
        assert resp.status_code == 200

        req = app._get_parameter_requirements ('pets')
        req ["ARGS"]["uid"]["required"] == True


def test_argspec_datamodel (app):
    @datamodel (MyModel)
    class Pet2:
        age: list
        color: str = 'any'
        num__lte = 200
        name__iregex = 'Han[sx]'
        def blankable__computed (context, val):
            return 'not blank'

    @app.route ("/pets2/<uid>")
    @app.argspec (Pet2)
    def pet2 (context, uid: str, **payload):
        return context.API ()

    def blankable__computed (context, val):
        return 'not blank'

    @app.route ("/pets5/<uid>")
    @app.argspec (datamodel = MyModel, num__lte = 200, name__iregex = 'Han[sx]', blankable = blankable__computed)
    def pets (context, uid: str, age: list, color: str = 'any', **payload):
        return context.API ()

    reqA = app._get_parameter_requirements ('pet2')
    reqB = app._get_parameter_requirements ('pets')
    assert 'type' not in reqB ['ARGS']
    assert 'tags' not in reqB ['ARGS']
    assert 'uid' in reqB ['ARGS']
    assert reqB ['ARGS']['uid']['required']
    assert reqA == reqB
    assert 'computed' in reqB ['ARGS']['blankable']

    assert reqB ['ARGS']['image']['type'] == 'file'
    assert 'len__lte' not in reqB ['ARGS']['image']

    @app.route ("/pets6/<uid>")
    @app.argspec (datamodel = MyModel, num__lte = 300, name__iregex = 'Han[sx]')
    def pet6 (context, uid: str, age: list, color: str = 'any', **payload):
        return context.API ()

    reqC = app._get_parameter_requirements ('pet6')
    assert 'computed' not in reqC ['ARGS']['blankable']
    assert reqC ['ARGS']['num']['lte'] == 300

    with app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.post ("/pets5", json = {"age": [1, 2], 'name': 'Hans', 'num': 100})
        assert resp.status_code == 404
        resp = cli.post ("pets5/400", json = {"age": [1, 2], 'name': 'Hans', 'num': 100})
        assert resp.status_code == 200

        resp = cli.post ("pets5/400", json = {"age": [1, 2], 'name': 'Hans', 'num': 100})
        assert resp.status_code == 200

        req = app._get_parameter_requirements ('pets')
        req ["ARGS"]["uid"]["required"] == True

