from django.db import models
from django.contrib.postgres.fields import ArrayField
from atila.collabo.django.model import AtilaModel

class Type (AtilaModel):
    name = models.CharField (max_length=16)

class Tag (AtilaModel):
    name = models.CharField (max_length=16)

class MyModel (AtilaModel):
    type = models.ForeignKey (Type, on_delete = models.CASCADE)
    tags = models.ManyToManyField (Tag)

    name = models.CharField (max_length=16)
    nullable = models.CharField (max_length=16, null = True, default = 'k')
    blankable = models.CharField (max_length=16, null = True, blank = True)
    choicable = models.CharField (max_length=16, null = True, blank = True, choices = [('b', 'b'), ('a', 'a')])

    # nums = ArrayField (models.IntegerField ())
    num = models.IntegerField ()
    in_date = models.DateTimeField (auto_now_add=True)
    up_date = models.DateTimeField (auto_now=True)

    text = models.TextField (null = True, blank = True)
    email = models.EmailField (null = True, blank = True)

    image = models.ImageField (null = True, blank = True, upload_to = '/tmp')
