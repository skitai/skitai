import os
import sys
if os.path.isdir ('../../atila/examples/dumbseek'):
    DUMBSEEK = '../../atila/examples/dumbseek'
else:
    DUMBSEEK = '../atila/examples/dumbseek'
sys.path.insert (0, DUMBSEEK)

import dumbseek
from dumbseek import analyzer
from dumbseek.index import indexer
from dumbseek.index import searcher
from dumbseek.index import db

def test_analyze ():
    assert analyzer.analyze ('Detective Holmes') == ['detective', 'holmes']

def test_index ():
    assert indexer.index ('Detective Holmes') == 0
    assert db.DOCUMENTS [0] == 'Detective Holmes'
    assert db.INVERTED_INDEX ['detective'] == {0}
    assert indexer.index ('Detective Monk') == 1
    assert db.INVERTED_INDEX ['monk'] == {1}
    assert searcher.search ('detective holmes') == ['Detective Holmes']

def test_api (launch, dryrun):
    serve = f'{DUMBSEEK}/skitaid.py'
    if not os.path.isfile (serve):
        return

    with launch (serve) as engine:
        r = engine.get ('/')
        assert r.json () ['app'] == dumbseek.NAME

        r = engine.post ('/api/tokens', data = {'query': 'Detective Holmes'})
        assert r.json () ['result'] == ['detective', 'holmes']

        r = engine.post ('/api/documents', data = {'document': 'Detective Holmes'})
        assert r.status_code == 201
        assert r.json () ['url'] == '/api/documents/0'

        r = engine.get ('/api/documents', params = {'q': 'Detective Holmes'})
        assert r.json () ['result'] == ['Detective Holmes']

        r = engine.get ('/api/documents', params = {'q': 'holmes'})
        assert r.json () ['result'] == ['Detective Holmes']

        r = engine.get ('/api/documents', params = {'q': 'detective holmes'})
        assert r.json () ['result'] == ['Detective Holmes']

        r = engine.get ('/api/documents/0')
        assert r.json () ['document'] == 'Detective Holmes'

        r = engine.post ('/api/documents', data = {'document': 'Detective Monk'})

        r = engine.get ('/api/documents', params = {'q': 'detective'})
        assert r.json () ['result'] == ['Detective Holmes', 'Detective Monk']