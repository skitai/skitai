from atila.app.parameters import ArgSpec, Parameters
from atila import file

class Fake (file):
    def __init__ (self):
        self.name = 'fakefile'
        self.size = 1000
        self.mimetype = 'text/fake'
        self.path = '/tmp/faskefile'


def test_parameter_class ():
    p = Parameters ()
    assert not p._validate_param ({'img': Fake ()}, files = ['img'])
    assert p._validate_param ({'img': 1}, files = ['img']) == 'parameter img should be a file'
    assert not p._validate_param ({'img': None}, files = ['img'])
    assert p._validate_param ({'img': None}, files = ['img'], required = ['img']) == 'parameter img is required'

    assert p._validate_param ({'img': Fake ()}, protected = ['img']) == 'parameter img is unknown'
    assert p._validate_param ({'img': Fake ()}, files = ['img'], required = ['img'], protected = ['img']) == 'parameter img is unknown'

    assert not p._validate_param ({'a': [1,2]}, lists = ['a'])
    d = {'a': '1,2'}
    assert not p._validate_param (d, lists = ['a'])
    assert d ['a'] == ['1', '2']
    assert not p._validate_param ({'a': '1|2'}, lists = ['a'])
    assert not p._validate_param ({'a': '1'}, lists = ['a'])
