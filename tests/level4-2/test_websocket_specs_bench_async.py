from websockets import connect
import time
import pytest
import asyncio

N = 100
CLIENTS = 8
DUE = []
G = 0

async def bench (ep):
    global DUE, G
    try:
        async with connect (f"ws://127.0.0.1:30371/websocket/bench/{ep}") as ws:
            s = time.time ()
            for _ in range (N):
                await ws.send("Hello, World")
                result = await ws.recv()
                assert result == "echo: Hello, World"
                G += 1
            DUE.append (time.time () - s)
    except asyncio.CancelledError:
        print ('webscoket cancelled')

async def clients (launch, ep):
    global G
    G = 0
    with launch ("./examples/websocket-spec.py") as engine:
        time.sleep (2)
        try:
            asyncio.create_task
        except AttributeError:
            _, pending = await asyncio.wait ([bench (ep) for _ in range (CLIENTS)], timeout = 30)
        else:
            _, pending = await asyncio.wait ([asyncio.create_task (bench (ep)) for _ in range (CLIENTS)], timeout = 30)

        [ it.cancel () for it in pending ]

        assert N * CLIENTS <= int (engine.get ("/websocket/bench/N").text) <= N * CLIENTS + CLIENTS
        assert G == int (N * CLIENTS)
        print ('*********** Bench result: {} {:2.3f} ({}, {})'.format (ep, sum (DUE), G, len (pending)))


@pytest.mark.asyncio
async def test_bench4 (launch):
    await clients (launch, 'async')

@pytest.mark.asyncio
async def test_bench7 (launch):
    await clients (launch, 'async_channel')

@pytest.mark.asyncio
async def test_bench2 (launch):
    await clients (launch, 'chatty')

@pytest.mark.asyncio
async def test_bench3 (launch):
    await clients (launch, 'session')

@pytest.mark.asyncio
async def test_bench6 (launch):
    await clients (launch, 'session_nopool')

