from django.apps import AppConfig


class FirebaseConfig(AppConfig):
    name = 'orm.firebase_vue'
