from __future__ import annotations
import skitai
import confutil
import pprint
import re
import os
from pydantic.dataclasses import dataclass

def test_response_model (app):
    os.environ ["ATILA_SET_SEPC"] = "1"

    class Pet:
        age: int
        color: str = 'any'

    @dataclass
    class Response:
        age: int
        color: str = 'any'

    @app.route ("/pets1", response = Response)
    @app.argspec (Pet)
    def pets (context, **payload):
        return context.API (age = payload ['age'])

    @app.route ("/pets2", response = Response)
    @app.argspec (Pet)
    def pets2 (context, **payload):
        return context.API (age = 'x')

    with app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.post ("pets1", json = {'age': 1, 'color': 'any'})
        assert resp.status_code == 200

        resp = cli.post ("pets2", json = {'age': 1, 'color': 'any'})
        assert resp.status_code == 502

    del os.environ ["ATILA_SET_SEPC"]
