import confutil

def test_response_header (app):
    @app.route ("/")
    def echo (context):
        context.response.append_header ("Vary", "a")
        return ""

    @app.route ("/1")
    def echo (context):
        context.response.append_header ("Vary", "a")
        return context.API ()

    @app.route ("/2")
    def echo (context):
        context.response.append_header ("X-Custom", "a")
        context.response.append_header ("X-Custom", "a")
        context.response.append_header ("X-Custom", "b")
        return context.API ()

    @app.route ("/3")
    def echo (context):
        context.response.append_header ("X-Custom", "a")
        context.response.append_header ("X-Custom", "a")
        context.response.append_header ("X-Custom", "b")
        context.response.set_header ("X-Custom", "c")
        return context.API ()

    @app.route ("/4")
    def echo (context):
        context.response.set_header ("X-Custom", "c")
        context.response.set_header ("X-Custom", "d")
        return context.API ()

    @app.route ("/5")
    def echo (context):
        context.cookie.set ("a", "b")
        context.cookie.set ("c", "d")
        return context.API ()

    with app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.get ("/")
        assert resp.status_code == 200
        assert resp.get_header ("Vary") == "a"

        resp = cli.get ("/1")
        assert resp.status_code == 200
        assert resp.get_header ("Vary") == "a, Accept"

        resp = cli.get ("/2")
        assert resp.status_code == 200
        assert resp.get_header ("X-Custom") == "a, b"

        resp = cli.get ("/3")
        assert resp.status_code == 200
        assert resp.get_header ("X-Custom") == "c"

        resp = cli.get ("/4")
        assert resp.status_code == 200
        assert resp.get_header ("X-Custom") == "d"

        resp = cli.get ("/5")
        assert resp.status_code == 200
        assert resp.headers ["Set-Cookie"] == ['a=b; path=/', 'c=d; path=/']

