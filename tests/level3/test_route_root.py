from atila import Atila
import confutil
import skitai
from rs4 import asyncore
import os
import time

def test_route_root (fscope_app, dbpath):
    @fscope_app.route ("/<path:path>")
    @fscope_app.route ("/")
    def index (context, path = None):
        return "Hello, World"

    with fscope_app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.get ("/")
        assert resp.text == "Hello, World"

        resp = cli.get ("/hello")
        assert resp.text == "Hello, World"

def test_route_ignore (fscope_app, dbpath):
    @fscope_app.route ("/<path:path>", ignore_pathes = ['admin'])
    def index2 (context, path = None):
        return "Hello, World"

    with fscope_app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.get ("/admin/xx")
        assert resp.status_code == 404

        resp = cli.get ("/api")
        assert resp.status_code == 200

def test_route_valid (fscope_app, dbpath):
    @fscope_app.route ("/<path:path>", valid_pathes = ['admin'])
    def index3 (context, path = None):
        return "Hello, World"

    with fscope_app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.get ("/")
        assert resp.status_code == 200

        resp = cli.get ("/admin/xx")
        assert resp.status_code == 200

        resp = cli.get ("/api")
        assert resp.status_code == 404

def test_route_both (fscope_app, dbpath):
    @fscope_app.route ("/<path:path>", valid_pathes = ['admin'], ignore_pathes = ['api'])
    def index3 (context, path = None):
        return "Hello, World"

    with fscope_app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.get ("/admin/xx")
        assert resp.status_code == 200

        resp = cli.get ("/api")
        assert resp.status_code == 404