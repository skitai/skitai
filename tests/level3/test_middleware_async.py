import confutil
import asyncio
import time



def test_async_middleware (fscope_app):
    app = fscope_app
    app.hooks_called = 0
    app.middleware_called = 0
    app.fired = 0

    @app.middleware ('perf2')
    def add_process_time2 (context, app, call_next):
        app.middleware_called += 1
        result = call_next ()
        context.response.set_header ("X-Process-Time2", 200)
        return result

    @app.middleware ('perf')
    async def add_process_time (context, app, call_next):
        app.middleware_called += 1
        result = await call_next ()
        context.response.set_header ("X-Process-Time", 100)
        return result

    @app.on ("FIRE")
    async def on_FIRE (context):
        app.fired += 1

    @app.before_request
    async def before_request (context):
        app.hooks_called += 1

    @app.teardown_request
    async def teardown_request (context):
        app.hooks_called += 1


    @app.route ("/1")
    async def a (context):
        app.emit ("FIRE")
        await asyncio.sleep (1)
        return "100"

    @app.route ("/2")
    def b (context, err = "no"):
        app.emit ("FIRE")
        return context.API (x = 100)


    N = 3
    with app.test_client ("/", confutil.getroot (), enable_async = True) as cli:
        for _ in range (N):
            resp = cli.get ("/1")
            assert resp.status_code == 200
            assert resp.text == "100"
            assert resp.get_header ("X-Process-Time") == '100'
            assert resp.get_header ("X-Process-Time2") == '200'

            resp = cli.get ("/2")
            assert resp.status_code == 200
            assert resp.json ()["x"] == 100
            assert resp.get_header ("X-Process-Time") == '100'
            assert resp.get_header ("X-Process-Time2") == '200'

    REQS = 2
    assert app.hooks_called == REQS * 2 * N
    assert app.middleware_called == 12
    assert app.fired == REQS * N



def test_async_middleware (fscope_app):
    app = fscope_app
    app.hooks_called = 0
    app.middleware_called = 0
    app.fired = 0

    @app.middleware ('perf2')
    def add_process_time2 (context, app, call_next):
        app.middleware_called += 1
        result = call_next ()
        context.response.set_header ("X-Process-Time2", 200)
        return result

    @app.middleware ('perf')
    async def add_process_time (context, app, call_next):
        app.middleware_called += 1
        result = await call_next ()
        context.response.set_header ("X-Process-Time", 100)
        return result


    @app.middleware ('perf3')
    async def add_process_time3 (context, app, call_next):
        app.middleware_called += 1
        result = await call_next ()
        context.response.set_header ("X-Process-Time3", 100)
        return result

    @app.middleware ('perf4')
    async def add_process_time4 (context, app, call_next):
        app.middleware_called += 1
        result = await call_next ()
        context.response.set_header ("X-Process-Time4", 100)
        return result

    @app.on ("FIRE")
    async def on_FIRE (context):
        app.fired += 1

    @app.before_request
    async def before_request (context):
        app.hooks_called += 1

    @app.teardown_request
    async def teardown_request (context):
        app.hooks_called += 1


    @app.route ("/1")
    async def a (context):
        app.emit ("FIRE")
        await asyncio.sleep (1)
        return "100"

    @app.route ("/2")
    def b (context, err = "no"):
        app.emit ("FIRE")
        return context.API (x = 100)

    N = 3
    with app.test_client ("/", confutil.getroot (), enable_async = True) as cli:
        for _ in range (N):
            resp = cli.get ("/1")
            assert resp.status_code == 200
            assert resp.text == "100"
            assert resp.get_header ("X-Process-Time") == '100'
            assert resp.get_header ("X-Process-Time2") == '200'

            resp = cli.get ("/2")
            assert resp.status_code == 200
            assert resp.json ()["x"] == 100
            assert resp.get_header ("X-Process-Time") == '100'
            assert resp.get_header ("X-Process-Time2") == '200'

    REQS = 2
    assert app.hooks_called == REQS * 2 * N
    assert app.middleware_called == N * 4 * 2
    assert app.fired == REQS * N