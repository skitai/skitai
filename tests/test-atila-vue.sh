set -xe

if [ -d ../../atila-vue ]
then
    pytest -x --disable-pytest-warnings level2/test_django_atila_model.py
    pytest -x --disable-pytest-warnings level5/test_atila_vue.py
fi
