set -xe

# pytest level4-1/test_upload.py::test_upload
# for i in {1..3}; do pytest -xs level4-3; done

pytest -x $1 $2 $3 level0
pytest -x $1 $2 $3 level1
pytest -x $1 $2 $3 level2
pytest -x $1 $2 $3 level3
pytest -x $1 $2 $3 level4
pytest -x $1 $2 $3 level4-1
pytest -x $1 $2 $3 level4-2
pytest -x $1 $2 $3 level5
