set -xe

rm -f /opt/skitai/.vscode && ln -s /opt/skitai/skitai/.vscode /opt/skitai/.vscode

cd "/opt/skitai"
if [ ! -d "atila" ]
then
    git clone git@gitlab.com:skitai/atila.git
fi
if [ ! -d "rs4" ]
then
    git clone git@gitlab.com:skitai/rs4.git
fi
if [ ! -d "sqlphile" ]
then
    git clone git@gitlab.com:skitai/sqlphile.git
fi
if [ ! -d "delune" ]
then
    git clone git@gitlab.com:skitai/atila-ext/delune.git
fi
if [ ! -d "atila-vue" ]
then
    git clone git@gitlab.com:atila-ext/atila-vue.git
fi
if [ ! -d "tfserver" ]
then
    git clone git@gitlab.com:skitai/atila-ext/tfserver.git
fi
if [ ! -d "dnn" ]
then
    git clone git@gitlab.com:skitai/dnn.git
fi

cd rs4 && pip3 install -e .
cd ../sqlphile && pip3 install -e .
cd ../skitai && pip3 install -e .
cd ../atila && pip3 install -e .
cd ../atila-vue && pip3 install -e .
cd ../dnn && pip3 install -e .
cd ../tfserver && pip3 install -e .
cd ../delune && pip3 install -e .

cd /opt/skitai/skitai/tools/benchmark/bench
./manage.py migrate
python3 init_db.py
