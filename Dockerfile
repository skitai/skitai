FROM hansroh/python:3.9

ARG DEBIAN_FRONTEND=noninteractive
ENV PYTHONUNBUFFERED=1

RUN ln -s /usr/local/bin/python3 /usr/bin/python3

LABEL title="Skitai Package Group Development"
LABEL author="hansroh"
LABEL version="1.0"

RUN pip3 install -U pip

COPY tools/benchmark/requirements.txt /requirements.txt
RUN pip3 install -Ur /requirements.txt && rm -f /requirements.txt

COPY tests/requirements.txt /requirements.txt
RUN pip3 install -Ur /requirements.txt && rm -f /requirements.txt

# compatible with python 3.9 ---------------------------
RUN pip3 install -U tensorflow-cpu==2.5.3 keras==2.4.3

# install dev tools --------------------
RUN apt update
RUN apt install -y wget gnupg unzip curl
RUN apt install -y build-essential zlib1g-dev cmake vim git tmux

RUN pip3 install -U twine typing-extensions

EXPOSE 5000
CMD [ "/bin/bash" ]
