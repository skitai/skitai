set -xe

cd ..
sudo rm -rf skitai--deletable
cp -r skitai skitai--deletable && rm -rf skitai--deletable/.git
docker run -it --rm -v ./skitai--deletable:/skitai --workdir /skitai hansroh/python:3.9 bash
